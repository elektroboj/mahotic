#include <string.h>
#include <stdint.h>

#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "simplerand.h"
#include "display.h"
#include "game.h"
#include "key.h"


#define MAX_LIGHT_PIXELS	(10*7/3-1)


static uint16_t game_speed;
static bool bird_died;
static uint16_t bird_ts;
static int16_t bird_y, bird_v;
static uint16_t game_ticks;
static uint8_t game_score;
static uint16_t scroll_ts;
static struct _pixels_t {
	uint8_t columns[10];
	uint8_t ofs;
	uint8_t area;
} pixels;
static struct _tube_new_t {
	uint8_t len;
	uint8_t wait;
	uint8_t asize, bsize;	// above/below size
} tube_new;


static const uint8_t img_bird_happy[] PROGMEM = {
	0b00000, 0b00000,
	0b00100, 0b00100,
	0b01010, 0b01010,
	0b00100, 0b00100,
	0b00000, 0b00000,
	0b01000, 0b00010,
	0b00111, 0b11100,
};

static const uint8_t img_bird_dead[] PROGMEM = {
	0b00000, 0b00000,
	0b01010, 0b01010,
	0b00100, 0b00100,
	0b01010, 0b01010,
	0b00000, 0b00000,
	0b00111, 0b11100,
	0b01000, 0b00010,
};

static const uint8_t img_hi_score[] PROGMEM = {
	0b10010, 0b01110,
	0b10010, 0b10000,
	0b11110, 0b01100,
	0b10010, 0b00010,
	0b10010, 0b11100,
	0b00000, 0b00000,
	0b10101, 0b01010,
};

static void pixels_init(void) {
	pixels.area = 0;
	pixels.ofs = 0;
	memset(pixels.columns, 0, sizeof(pixels.columns));
}

static void pixels_push(uint8_t num) {
	++pixels.ofs;
	if (pixels.ofs >= sizeof(pixels.columns))
		pixels.ofs = 0;
	pixels.area = pixels.area - pixels.columns[pixels.ofs] + num;
	pixels.columns[pixels.ofs] = num;
}

static uint8_t pixels_peek(uint8_t history) {
	int8_t ofs = (int8_t)pixels.ofs - (int8_t)history;
	if (ofs < 0) ofs += sizeof(pixels.columns);
	return pixels.columns[ofs];
}

static uint8_t game_tubes_calc_min_wait(void) {
	uint8_t tube_area = tube_new.len * (tube_new.asize + tube_new.bsize);
	uint8_t ofs;
	// how many more columns can we keep on the display
	// along with this new tube?
	for (ofs = 0; ofs < 9; ++ofs) {
		tube_area += pixels_peek(ofs);
		if (tube_area > MAX_LIGHT_PIXELS)
			break;
	}
	return 9 - ofs;
}

static void game_tubes_design(void) {
	uint8_t hole_size, max_len, min_wait, max_ofs;
	int8_t ofs;

	// choose hole size
	hole_size = (simplerand() % 4) + 2;
	// choose tube len
	max_len = MAX_LIGHT_PIXELS / (7 - hole_size);
	if (max_len > 8) max_len = 8;
	tube_new.len = (simplerand() % (max_len + 10)) + 1;
	if (tube_new.len > max_len) tube_new.len = 1;	// slightly preferring len=1
	// choose tube distance (wait after the previous one)
	min_wait = game_tubes_calc_min_wait();
	tube_new.wait = simplerand() % 5;
	if (tube_new.wait < min_wait) tube_new.wait = min_wait;
	// choose hole offset
	max_ofs = tube_new.wait + 1;
	ofs = (tube_new.asize + (7 - tube_new.bsize)) / 2;	// prev center
	ofs += (int8_t)(simplerand() % max_ofs) - (int8_t)max_ofs / 2;	// new center
	ofs -= hole_size / 2;	// translate to topmost hole pixel
	if (ofs < 0) ofs = 0;
	else if (ofs + hole_size > 7) ofs = 7 - hole_size;
	// calc asize && bsize
	tube_new.asize = ofs;
	tube_new.bsize = 7 - ofs - hole_size;
}

static void game_tubes_init(void) {
	tube_new.asize = tube_new.bsize = 3;
	pixels_init();
	game_tubes_design();
	tube_new.wait = 0;
}

static uint8_t game_tubes_draw(void) {
	uint8_t y;
	for (y = 0; y < tube_new.asize; ++y)
		display_set_pix(9, y, true);
	for (y = 7 - tube_new.bsize; y < 7; ++y)
		display_set_pix(9, y, true);
	return tube_new.asize + tube_new.bsize;
}

static void game_tubes_factory(void) {
	uint8_t add_area = 0;

	if (tube_new.wait > 0) {
		--tube_new.wait;
	} else {
		add_area = game_tubes_draw();
		--tube_new.len;
		if (!tube_new.len)
			game_tubes_design();
	}

	pixels_push(add_area);
}

static void game_bird_init(void) {
	bird_y = 0x300;
	bird_ts = game_ticks;
	bird_died = false;
}

static void game_bird_draw(int16_t y, bool light) {
	y >>= 8;
	if (y >= 0 && y <= 6) {
		display_set_pix(0, y, light);
	}
}

static bool game_bird_check_collision(void) {
	bird_died = display_get_pix(0, bird_y >> 8);
	return bird_died;
}

static void game_bird_control(void) {
	int8_t bird_y_disp_prev = bird_y >> 8;
	int8_t bird_y_disp_new;

	// gravity
	if (game_ticks - bird_ts > 50) {
		bird_v += 7;
		bird_y += bird_v;
		bird_ts = game_ticks;
	}

	// wings
	if (key_is_clicked()) {
		bird_y -= 0x100;
		bird_v = 0;
	}

	// clipping
	if (bird_y >= 0x600) {
		bird_y = 0x600;
		bird_v = 0;
	} else if (bird_y < 0) {
		bird_y = 0;
	}

	// if pixel moved
	// check for colisions and draw
	bird_y_disp_new = bird_y >> 8;
	if (bird_y_disp_new != bird_y_disp_prev && !game_bird_check_collision()) {
		game_bird_draw(bird_y_disp_prev << 8, false);
		game_bird_draw(bird_y, true);
	}
}

static void game_scroll_init(void) {
	scroll_ts = game_ticks;
	game_tubes_init();
}

static void game_count_points(void) {
	if ( (display_get_pix(0, 0) || display_get_pix(0, 6)) &&
		!(display_get_pix(1, 0) || display_get_pix(1, 6))) {
		++game_score;
		if (game_score > 99)
			game_score = 99;
		if (game_speed > 50)
			game_speed -= 4;
	}
}

static void game_scroll(void) {
	if (game_ticks - scroll_ts > game_speed) {
		display_scroll();
		if (game_bird_check_collision())
			return;
		game_count_points();
		game_bird_draw(bird_y, true);	// restore bird after scrolling
		game_tubes_factory();
		scroll_ts = game_ticks;
	}
}

static void game_block(uint16_t duration) {
	game_ticks = display_ticks();
	while (display_ticks() - game_ticks < duration);
}

static void game_intro(void) {
	display_image(img_bird_happy);
	game_block(1000);
}

static void game_over(void) {
	uint8_t prev_hi_score;

	prev_hi_score = eeprom_read_byte(0);
	if (prev_hi_score > 99)
		prev_hi_score = 0;

	// display dead bird and the current score
	display_image(img_bird_dead);
	game_block(1000);
	display_number(game_score);
	game_block(1000);

	// display previous hi-score
	display_image(img_hi_score);
	game_block(1000);
	display_number(prev_hi_score);
	game_block(1000);

	// store the new hi score if required
	if (game_score > prev_hi_score) {
		eeprom_write_byte(0, game_score);
	}
}

void game(void) {
	game_intro();
	display_clear();

	game_score = 0;
	game_speed = 300;
	game_ticks = display_ticks();
	game_bird_init();
	game_scroll_init();

	while (!bird_died) {
		game_ticks = display_ticks();
		game_bird_control();
		game_scroll();
	}

	game_over();
}
