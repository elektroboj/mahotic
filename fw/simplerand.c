#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

static uint16_t seed __attribute__((section (".noinit")));
extern const uint16_t __data_start, __stack;

#if 0
uint16_t simplerand(void) {
	seed = seed * 2053 + 13849;
	return seed;
}
#endif

#if 0
uint16_t simplerand(void) {
	// magic=27963: len=65534
	// magic=63465: len=21844*3
	bool carry = seed & 0x8000;
	seed <<= 1;
	if (carry) seed ^= 63465;
	return seed;
}
#endif

#if 1
#include <util/crc16.h>

uint16_t simplerand(void) {
	if (seed == 0 || seed == 63503)
		++seed;
	seed = _crc_ccitt_update(seed, 0);
	return seed;
}
#endif

void init_seed(void) __attribute__((section(".init3"), naked, used));
void init_seed(void) {
	register const uint16_t *mem;
	register uint16_t memxor;
	// note: memxor is intentionally not initialized
	// avr-gcc 5.4.0 with LTO turned on does not complain to that for some reason...
	for (mem = &__data_start; mem < &__stack; ++mem)
		memxor ^= *mem;
	seed = memxor;
}
