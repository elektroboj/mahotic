#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/io.h>

#include "display.h"


static uint8_t disp_buf[16];
static uint8_t disp_buf_i = 0;
static volatile uint16_t disp_ticks;

static const uint8_t disp_font[] PROGMEM = {
	// 0
	0b01100,
	0b10010,
	0b10010,
	0b10010,
	0b10010,
	0b01100,
	0b00000,

	// 1
	0b00100,
	0b01100,
	0b10100,
	0b00100,
	0b00100,
	0b11110,
	0b00000,

	// 2
	0b01100,
	0b10010,
	0b00010,
	0b00100,
	0b01000,
	0b11110,
	0b00000,

	// 3
	0b01100,
	0b10010,
	0b00100,
	0b00010,
	0b10010,
	0b01100,
	0b00000,

	// 4
	0b01000,
	0b10000,
	0b10100,
	0b11110,
	0b00100,
	0b00100,
	0b00000,

	// 5
	0b11110,
	0b10000,
	0b11000,
	0b00100,
	0b10010,
	0b01100,
	0b00000,

	// 6
	0b01100,
	0b10010,
	0b10000,
	0b11100,
	0b10010,
	0b01100,
	0b00000,

	// 7
	0b11110,
	0b10010,
	0b00010,
	0b00100,
	0b00100,
	0b00100,
	0b00000,

	// 8
	0b01100,
	0b10010,
	0b01100,
	0b10010,
	0b10010,
	0b01100,
	0b00000,

	// 9
	0b01100,
	0b10010,
	0b01110,
	0b00010,
	0b10010,
	0b01100,
	0b00000,
};

static inline void disp_shreg_data(bool data) __attribute__((always_inline));
static inline void disp_shreg_data(bool data) {
	if (data) {
		PORTB |=  _BV(PB2);
	} else {
		PORTB &= ~_BV(PB2);
	}
}

static inline void disp_shreg_cp(void) __attribute__((always_inline));
static inline void disp_shreg_cp(void) {
	PORTB &= ~_BV(PB0);
	PORTB |=  _BV(PB0);
}

ISR(TIM0_COMPA_vect) {
	disp_shreg_data(disp_buf_i);
	PORTA = 0x00;
	disp_shreg_cp();
	PORTA = disp_buf[disp_buf_i];
	disp_buf_i = (disp_buf_i + 1) & 15;
	++disp_ticks;
}

void display_start(void) {
	uint8_t i;

	// init ports
	PORTA = 0x00;
	DDRA = 0xFF;
	DDRB |= 0x05;

	// init display
	disp_buf_i = 0;
	disp_shreg_data(true);
	for (i = 0; i < 16; ++i) {
		disp_shreg_cp();
	}
	display_clear();

	// init and start timer
	PRR &= ~_BV(PRTIM0);
	OCR0A = 15;
	TCNT0 = 0;
	TCCR0A = _BV(WGM01);
	TCCR0B = _BV(CS01) | _BV(CS00);
	TIFR0 |= _BV(OCF0A);
	TIMSK0 |= _BV(OCIE0A);
}

void display_stop(void) {
	TCCR0B = 0x00;
	PORTA = 0x00;
	PRR |= _BV(PRTIM0);
}

void display_clear(void) {
	memset(disp_buf, 0, 2*7);
}

void display_scroll(void) {
	uint8_t y;
	for (y = 0; y < 7; ++y) {
		disp_buf[y] <<= 1;
		disp_buf[y+7] <<= 1;
		disp_buf[y] |= !!(disp_buf[y+7] & 0x20);
	}
}

static bool display_get_pix_loc(uint8_t x, uint8_t y, uint8_t **loc, uint8_t *mask)
{
	// sanity checks
	if (x >= 10 || y >= 7)
		return false;

	// account for split-display formation
	*loc = disp_buf;
	if (x >= 5) {
		x -= 5;
		*loc += 7;
	}

	// get to the right 5-pix segment
	*loc += y;

	// generate mask
	*mask = 1 << (4 - x);

	return true;
}

void display_set_pix(uint8_t x, uint8_t y, bool light) {
	uint8_t *buf, mask;

	// determine location
	if (!display_get_pix_loc(x, y, &buf, &mask))
		return;

	// paint!
	if (light) {
		*buf |=  mask;
	} else {
		*buf &= ~mask;
	}
}

bool display_get_pix(uint8_t x, uint8_t y) {
	uint8_t *buf, mask;

	// determine location
	if (!display_get_pix_loc(x, y, &buf, &mask))
		return false;

	// peek
	return !!(*buf & mask);
}

uint16_t display_ticks(void) {
	uint16_t tmp_ticks;
	uint8_t orig_timsk0 = TIMSK0;
	TIMSK0 &= ~_BV(OCIE0A);
	tmp_ticks = disp_ticks;
	TIMSK0 = orig_timsk0;
	return tmp_ticks;
}

void display_digit(uint8_t pos, uint8_t dig) {
	uint8_t *buf;
	if (dig <= 9 && pos <= 1) {
		buf = disp_buf + (pos ? 0 : 7);
		memcpy_P(buf, disp_font + dig * 7, 7);
	}
}

void display_number(uint8_t num) {
	display_digit(0, num % 10);
	display_digit(1, num / 10);
}

void display_image(const uint8_t *img) {
	const uint8_t *img_seg;
	uint8_t i;
	img_seg = img;
	for (i = 0; i <  7; ++i, img_seg += 2)
		disp_buf[i] = pgm_read_byte(img_seg);
	img_seg = img + 1;
	for (i = 7; i < 14; ++i, img_seg += 2)
		disp_buf[i] = pgm_read_byte(img_seg);
}
