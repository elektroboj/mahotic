#include <avr/interrupt.h>
#include <avr/io.h>

#include "display.h"
#include "key.h"


static uint16_t key_ts;
static bool key_state_cur = false;

void key_init(void) {
	DDRB &= ~_BV(1);
	PORTB |= _BV(1);
	GIMSK |= _BV(PCIE1);
}

bool key_state_raw(void) {
	return bit_is_clear(PINB, 1);
}

bool key_is_pressed(void) {
	key_is_clicked();
	return key_state_cur;
}

bool key_is_clicked(void) {
	uint16_t cur_ts = display_ticks();
	if (key_state_cur == key_state_raw()) {
		key_ts = cur_ts;
	} else if (cur_ts - key_ts > 10) {
		key_state_cur = !key_state_cur;
		return key_state_cur;
	}
	return false;
}

ISR(PCINT1_vect) {
}

void key_enable_wakeup(bool en_wku) {
	if (en_wku) {
		GIFR   |=  _BV(PCIF1);	// clear interrupt flag
		PCMSK1 |=  _BV(PCINT9);
	} else {
		PCMSK1 &= ~_BV(PCINT9);
	}
}
