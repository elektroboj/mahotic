#include <stdbool.h>

#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/io.h>

#include "key.h"


void take_it_easy(void) {
	// disable analog comparator
	ACSR |= _BV(ACD);

	// disable TIM1, TIM0, USI and ADC
	// note that TIM0 will be powered up/down along with the display
	PRR = _BV(PRTIM1) | _BV(PRTIM0) | _BV(PRUSI) | _BV(PRADC);
}

static bool is_time_to_wake_up(void) {
	uint16_t tmr;
	for (tmr = 0; tmr < 1500; ++tmr) {
		_delay_ms(1);
		if (!key_state_raw())
			return false;
	}
	return true;
}

void power_down(void) {
	key_enable_wakeup(true);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	// wait for key release
	while (key_state_raw()) {
		sleep_mode();
	}
	// wait for wakeup
	do {
		sleep_mode();
	} while (!is_time_to_wake_up());
	key_enable_wakeup(false);
}
