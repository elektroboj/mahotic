#ifndef __KEY_H__
#define __KEY_H__

#include <stdbool.h>


void key_init(void);
bool key_state_raw(void);	// no debouncing
void key_enable_wakeup(bool en_wku);

// note: these functions must be polled!
//       and they are using display_ticks, which requires display to be enabled
bool key_is_clicked(void);
bool key_is_pressed(void);

#endif
