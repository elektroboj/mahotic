#include <avr/interrupt.h>

#include "display.h"
#include "sleep.h"
#include "game.h"
#include "key.h"


static void eat() {
	display_start();
	key_init();
}

static void sleep() {
	display_stop();
	power_down();
}

int main() {
	take_it_easy();
	sei();

	for (;;) {
		eat();
		game();
		sleep();
	}

	return 0;
}
