#ifndef __SLEEP_H__
#define __SLEEP_H__

void take_it_easy(void);
void power_down(void);

#endif
