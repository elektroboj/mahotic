#ifndef __SIMPLERAND_H__
#define __SIMPLERAND_H__

#include <stdint.h>

uint16_t simplerand(void);

#endif
