#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdbool.h>
#include <stdint.h>

void display_start(void);
void display_stop(void);
void display_clear(void);
void display_scroll(void);
void display_set_pix(uint8_t x, uint8_t y, bool light);
bool display_get_pix(uint8_t x, uint8_t y);
uint16_t display_ticks(void);
void display_digit(uint8_t pos, uint8_t dig);
void display_number(uint8_t num);
void display_image(const uint8_t *img);

#endif
