# this is a calculator for a single LED current

max_cur = 5e-3
num_leds = 5*14
num_cycles = 16

def dump(p, avg_cur):
	print 'Percent: %.1f%%' % (p * 100)
	print 'AvgCurr: %.3fmA' % (avg_cur * 1e3)
	print 'PkCurr:  %.3fmA' % (avg_cur * num_cycles * 1e3)


print 'Average current from percentage:'
p = 0.3
avg_cur = max_cur / num_leds / p
dump(p, avg_cur)
print

print 'Percentage from average current:'
avg_cur = 1e-3
p = max_cur / num_leds / avg_cur
dump(p, avg_cur)
