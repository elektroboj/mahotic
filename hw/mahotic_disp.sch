<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Estudent-radionica">
<packages>
<package name="LED_MATRIX_PACKAGE">
<pad name="6" x="0" y="0" drill="0.8" diameter="1.4224"/>
<pad name="5" x="0" y="2.54" drill="0.8" diameter="1.4224"/>
<pad name="4" x="0" y="5.08" drill="0.8" diameter="1.4224"/>
<pad name="3" x="0" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="2" x="0" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="1" x="0" y="12.7" drill="0.8" diameter="1.4224" shape="square"/>
<pad name="7" x="7.62" y="0" drill="0.8" diameter="1.4224"/>
<pad name="8" x="7.62" y="2.54" drill="0.8" diameter="1.4224"/>
<pad name="9" x="7.62" y="5.08" drill="0.8" diameter="1.4224"/>
<pad name="10" x="7.62" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="11" x="7.62" y="10.16" drill="0.8" diameter="1.4224"/>
<pad name="12" x="7.62" y="12.7" drill="0.8" diameter="1.4224"/>
<wire x1="-2.54" y1="-2.59" x2="10.16" y2="-2.59" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-2.59" x2="-2.54" y2="15.21" width="0.3048" layer="21"/>
<wire x1="10.16" y1="-2.59" x2="10.16" y2="15.21" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="15.21" x2="10.16" y2="15.21" width="0.3048" layer="21"/>
<circle x="-1.27" y="-1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="-1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="-1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="-1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="-1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="1.27" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="3.81" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="3.81" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="3.81" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="3.81" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="3.81" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="6.35" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="6.35" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="6.35" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="6.35" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="6.35" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="8.89" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="8.89" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="8.89" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="8.89" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="8.89" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="11.43" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="11.43" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="11.43" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="11.43" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="11.43" radius="0.95" width="0.127" layer="51"/>
<circle x="-1.27" y="13.97" radius="0.95" width="0.127" layer="51"/>
<circle x="1.27" y="13.97" radius="0.95" width="0.127" layer="51"/>
<circle x="3.81" y="13.97" radius="0.95" width="0.127" layer="51"/>
<circle x="6.35" y="13.97" radius="0.95" width="0.127" layer="51"/>
<circle x="8.89" y="13.97" radius="0.95" width="0.127" layer="51"/>
<text x="-2.54" y="15.875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LED_MATRIX_SYMBOL">
<pin name="1" x="5.08" y="25.4" length="short" rot="R270"/>
<pin name="3" x="10.16" y="25.4" length="short" rot="R270"/>
<pin name="10" x="15.24" y="25.4" length="short" rot="R270"/>
<pin name="7" x="20.32" y="25.4" length="short" rot="R270"/>
<pin name="8" x="25.4" y="25.4" length="short" rot="R270"/>
<wire x1="1.27" y1="19.05" x2="2.54" y2="19.05" width="0.254" layer="94"/>
<wire x1="2.54" y1="19.05" x2="3.81" y2="19.05" width="0.254" layer="94"/>
<wire x1="3.81" y1="19.05" x2="2.54" y2="17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.78" x2="1.27" y2="19.05" width="0.254" layer="94"/>
<wire x1="1.27" y1="17.78" x2="2.54" y2="17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.78" x2="3.81" y2="17.78" width="0.254" layer="94"/>
<wire x1="6.35" y1="19.05" x2="7.62" y2="19.05" width="0.254" layer="94"/>
<wire x1="7.62" y1="19.05" x2="8.89" y2="19.05" width="0.254" layer="94"/>
<wire x1="8.89" y1="19.05" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="6.35" y2="19.05" width="0.254" layer="94"/>
<wire x1="6.35" y1="17.78" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="8.89" y2="17.78" width="0.254" layer="94"/>
<wire x1="11.43" y1="19.05" x2="12.7" y2="19.05" width="0.254" layer="94"/>
<wire x1="12.7" y1="19.05" x2="13.97" y2="19.05" width="0.254" layer="94"/>
<wire x1="13.97" y1="19.05" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="11.43" y2="19.05" width="0.254" layer="94"/>
<wire x1="11.43" y1="17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="13.97" y2="17.78" width="0.254" layer="94"/>
<wire x1="16.51" y1="19.05" x2="17.78" y2="19.05" width="0.254" layer="94"/>
<wire x1="17.78" y1="19.05" x2="19.05" y2="19.05" width="0.254" layer="94"/>
<wire x1="19.05" y1="19.05" x2="17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="16.51" y2="19.05" width="0.254" layer="94"/>
<wire x1="16.51" y1="17.78" x2="17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="19.05" y2="17.78" width="0.254" layer="94"/>
<wire x1="21.59" y1="19.05" x2="22.86" y2="19.05" width="0.254" layer="94"/>
<wire x1="22.86" y1="19.05" x2="24.13" y2="19.05" width="0.254" layer="94"/>
<wire x1="24.13" y1="19.05" x2="22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="22.86" y1="17.78" x2="21.59" y2="19.05" width="0.254" layer="94"/>
<wire x1="21.59" y1="17.78" x2="22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="22.86" y1="17.78" x2="24.13" y2="17.78" width="0.254" layer="94"/>
<wire x1="1.27" y1="11.43" x2="2.54" y2="11.43" width="0.254" layer="94"/>
<wire x1="2.54" y1="11.43" x2="3.81" y2="11.43" width="0.254" layer="94"/>
<wire x1="3.81" y1="11.43" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="1.27" y2="11.43" width="0.254" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="94"/>
<wire x1="6.35" y1="11.43" x2="7.62" y2="11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="11.43" x2="8.89" y2="11.43" width="0.254" layer="94"/>
<wire x1="8.89" y1="11.43" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="6.35" y2="11.43" width="0.254" layer="94"/>
<wire x1="6.35" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="8.89" y2="10.16" width="0.254" layer="94"/>
<wire x1="11.43" y1="11.43" x2="12.7" y2="11.43" width="0.254" layer="94"/>
<wire x1="12.7" y1="11.43" x2="13.97" y2="11.43" width="0.254" layer="94"/>
<wire x1="13.97" y1="11.43" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="11.43" y2="11.43" width="0.254" layer="94"/>
<wire x1="11.43" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="13.97" y2="10.16" width="0.254" layer="94"/>
<wire x1="16.51" y1="11.43" x2="17.78" y2="11.43" width="0.254" layer="94"/>
<wire x1="17.78" y1="11.43" x2="19.05" y2="11.43" width="0.254" layer="94"/>
<wire x1="19.05" y1="11.43" x2="17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="10.16" x2="16.51" y2="11.43" width="0.254" layer="94"/>
<wire x1="16.51" y1="10.16" x2="17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="10.16" x2="19.05" y2="10.16" width="0.254" layer="94"/>
<wire x1="21.59" y1="11.43" x2="22.86" y2="11.43" width="0.254" layer="94"/>
<wire x1="22.86" y1="11.43" x2="24.13" y2="11.43" width="0.254" layer="94"/>
<wire x1="24.13" y1="11.43" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="21.59" y2="11.43" width="0.254" layer="94"/>
<wire x1="21.59" y1="10.16" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<pin name="12" x="-2.54" y="15.24" length="short"/>
<pin name="11" x="-2.54" y="7.62" length="short"/>
<pin name="2" x="-2.54" y="0" length="short"/>
<pin name="9" x="-2.54" y="-7.62" length="short"/>
<pin name="4" x="-2.54" y="-15.24" length="short"/>
<pin name="5" x="-2.54" y="-22.86" length="short"/>
<pin name="6" x="-2.54" y="-30.48" length="short"/>
<wire x1="22.86" y1="10.16" x2="24.13" y2="10.16" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="3.81" x2="7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="3.81" x2="8.89" y2="3.81" width="0.254" layer="94"/>
<wire x1="8.89" y1="3.81" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="6.35" y2="3.81" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="8.89" y2="2.54" width="0.254" layer="94"/>
<wire x1="11.43" y1="3.81" x2="12.7" y2="3.81" width="0.254" layer="94"/>
<wire x1="12.7" y1="3.81" x2="13.97" y2="3.81" width="0.254" layer="94"/>
<wire x1="13.97" y1="3.81" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="11.43" y2="3.81" width="0.254" layer="94"/>
<wire x1="11.43" y1="2.54" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="13.97" y2="2.54" width="0.254" layer="94"/>
<wire x1="16.51" y1="3.81" x2="17.78" y2="3.81" width="0.254" layer="94"/>
<wire x1="17.78" y1="3.81" x2="19.05" y2="3.81" width="0.254" layer="94"/>
<wire x1="19.05" y1="3.81" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="16.51" y2="3.81" width="0.254" layer="94"/>
<wire x1="16.51" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="19.05" y2="2.54" width="0.254" layer="94"/>
<wire x1="21.59" y1="3.81" x2="22.86" y2="3.81" width="0.254" layer="94"/>
<wire x1="22.86" y1="3.81" x2="24.13" y2="3.81" width="0.254" layer="94"/>
<wire x1="24.13" y1="3.81" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="2.54" x2="21.59" y2="3.81" width="0.254" layer="94"/>
<wire x1="21.59" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="2.54" x2="24.13" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="3.81" y2="-3.81" width="0.254" layer="94"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="8.89" y2="-3.81" width="0.254" layer="94"/>
<wire x1="8.89" y1="-3.81" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="6.35" y2="-3.81" width="0.254" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="11.43" y1="-3.81" x2="12.7" y2="-3.81" width="0.254" layer="94"/>
<wire x1="12.7" y1="-3.81" x2="13.97" y2="-3.81" width="0.254" layer="94"/>
<wire x1="13.97" y1="-3.81" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="11.43" y2="-3.81" width="0.254" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="13.97" y2="-5.08" width="0.254" layer="94"/>
<wire x1="16.51" y1="-3.81" x2="17.78" y2="-3.81" width="0.254" layer="94"/>
<wire x1="17.78" y1="-3.81" x2="19.05" y2="-3.81" width="0.254" layer="94"/>
<wire x1="19.05" y1="-3.81" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="16.51" y2="-3.81" width="0.254" layer="94"/>
<wire x1="16.51" y1="-5.08" x2="17.78" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="19.05" y2="-5.08" width="0.254" layer="94"/>
<wire x1="21.59" y1="-3.81" x2="22.86" y2="-3.81" width="0.254" layer="94"/>
<wire x1="22.86" y1="-3.81" x2="24.13" y2="-3.81" width="0.254" layer="94"/>
<wire x1="24.13" y1="-3.81" x2="22.86" y2="-5.08" width="0.254" layer="94"/>
<wire x1="22.86" y1="-5.08" x2="21.59" y2="-3.81" width="0.254" layer="94"/>
<wire x1="21.59" y1="-5.08" x2="22.86" y2="-5.08" width="0.254" layer="94"/>
<wire x1="22.86" y1="-5.08" x2="24.13" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="-11.43" x2="2.54" y2="-11.43" width="0.254" layer="94"/>
<wire x1="2.54" y1="-11.43" x2="3.81" y2="-11.43" width="0.254" layer="94"/>
<wire x1="3.81" y1="-11.43" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="1.27" y2="-11.43" width="0.254" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="3.81" y2="-12.7" width="0.254" layer="94"/>
<wire x1="6.35" y1="-11.43" x2="7.62" y2="-11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="-11.43" x2="8.89" y2="-11.43" width="0.254" layer="94"/>
<wire x1="8.89" y1="-11.43" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="6.35" y2="-11.43" width="0.254" layer="94"/>
<wire x1="6.35" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="8.89" y2="-12.7" width="0.254" layer="94"/>
<wire x1="11.43" y1="-11.43" x2="12.7" y2="-11.43" width="0.254" layer="94"/>
<wire x1="12.7" y1="-11.43" x2="13.97" y2="-11.43" width="0.254" layer="94"/>
<wire x1="13.97" y1="-11.43" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="11.43" y2="-11.43" width="0.254" layer="94"/>
<wire x1="11.43" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="13.97" y2="-12.7" width="0.254" layer="94"/>
<wire x1="16.51" y1="-11.43" x2="17.78" y2="-11.43" width="0.254" layer="94"/>
<wire x1="17.78" y1="-11.43" x2="19.05" y2="-11.43" width="0.254" layer="94"/>
<wire x1="19.05" y1="-11.43" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="16.51" y2="-11.43" width="0.254" layer="94"/>
<wire x1="16.51" y1="-12.7" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="19.05" y2="-12.7" width="0.254" layer="94"/>
<wire x1="21.59" y1="-11.43" x2="22.86" y2="-11.43" width="0.254" layer="94"/>
<wire x1="22.86" y1="-11.43" x2="24.13" y2="-11.43" width="0.254" layer="94"/>
<wire x1="24.13" y1="-11.43" x2="22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="21.59" y2="-11.43" width="0.254" layer="94"/>
<wire x1="21.59" y1="-12.7" x2="22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="24.13" y2="-12.7" width="0.254" layer="94"/>
<wire x1="1.27" y1="-19.05" x2="2.54" y2="-19.05" width="0.254" layer="94"/>
<wire x1="2.54" y1="-19.05" x2="3.81" y2="-19.05" width="0.254" layer="94"/>
<wire x1="3.81" y1="-19.05" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-19.05" width="0.254" layer="94"/>
<wire x1="1.27" y1="-20.32" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="3.81" y2="-20.32" width="0.254" layer="94"/>
<wire x1="6.35" y1="-19.05" x2="7.62" y2="-19.05" width="0.254" layer="94"/>
<wire x1="7.62" y1="-19.05" x2="8.89" y2="-19.05" width="0.254" layer="94"/>
<wire x1="8.89" y1="-19.05" x2="7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="6.35" y2="-19.05" width="0.254" layer="94"/>
<wire x1="6.35" y1="-20.32" x2="7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="8.89" y2="-20.32" width="0.254" layer="94"/>
<wire x1="11.43" y1="-19.05" x2="12.7" y2="-19.05" width="0.254" layer="94"/>
<wire x1="12.7" y1="-19.05" x2="13.97" y2="-19.05" width="0.254" layer="94"/>
<wire x1="13.97" y1="-19.05" x2="12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="11.43" y2="-19.05" width="0.254" layer="94"/>
<wire x1="11.43" y1="-20.32" x2="12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="13.97" y2="-20.32" width="0.254" layer="94"/>
<wire x1="16.51" y1="-19.05" x2="17.78" y2="-19.05" width="0.254" layer="94"/>
<wire x1="17.78" y1="-19.05" x2="19.05" y2="-19.05" width="0.254" layer="94"/>
<wire x1="19.05" y1="-19.05" x2="17.78" y2="-20.32" width="0.254" layer="94"/>
<wire x1="17.78" y1="-20.32" x2="16.51" y2="-19.05" width="0.254" layer="94"/>
<wire x1="16.51" y1="-20.32" x2="17.78" y2="-20.32" width="0.254" layer="94"/>
<wire x1="17.78" y1="-20.32" x2="19.05" y2="-20.32" width="0.254" layer="94"/>
<wire x1="21.59" y1="-19.05" x2="22.86" y2="-19.05" width="0.254" layer="94"/>
<wire x1="22.86" y1="-19.05" x2="24.13" y2="-19.05" width="0.254" layer="94"/>
<wire x1="24.13" y1="-19.05" x2="22.86" y2="-20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="-20.32" x2="21.59" y2="-19.05" width="0.254" layer="94"/>
<wire x1="21.59" y1="-20.32" x2="22.86" y2="-20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="-20.32" x2="24.13" y2="-20.32" width="0.254" layer="94"/>
<wire x1="1.27" y1="-26.67" x2="2.54" y2="-26.67" width="0.254" layer="94"/>
<wire x1="2.54" y1="-26.67" x2="3.81" y2="-26.67" width="0.254" layer="94"/>
<wire x1="3.81" y1="-26.67" x2="2.54" y2="-27.94" width="0.254" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="1.27" y2="-26.67" width="0.254" layer="94"/>
<wire x1="1.27" y1="-27.94" x2="2.54" y2="-27.94" width="0.254" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="3.81" y2="-27.94" width="0.254" layer="94"/>
<wire x1="6.35" y1="-26.67" x2="7.62" y2="-26.67" width="0.254" layer="94"/>
<wire x1="7.62" y1="-26.67" x2="8.89" y2="-26.67" width="0.254" layer="94"/>
<wire x1="8.89" y1="-26.67" x2="7.62" y2="-27.94" width="0.254" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="6.35" y2="-26.67" width="0.254" layer="94"/>
<wire x1="6.35" y1="-27.94" x2="7.62" y2="-27.94" width="0.254" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="8.89" y2="-27.94" width="0.254" layer="94"/>
<wire x1="11.43" y1="-26.67" x2="12.7" y2="-26.67" width="0.254" layer="94"/>
<wire x1="12.7" y1="-26.67" x2="13.97" y2="-26.67" width="0.254" layer="94"/>
<wire x1="13.97" y1="-26.67" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="11.43" y2="-26.67" width="0.254" layer="94"/>
<wire x1="11.43" y1="-27.94" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="13.97" y2="-27.94" width="0.254" layer="94"/>
<wire x1="16.51" y1="-26.67" x2="17.78" y2="-26.67" width="0.254" layer="94"/>
<wire x1="17.78" y1="-26.67" x2="19.05" y2="-26.67" width="0.254" layer="94"/>
<wire x1="19.05" y1="-26.67" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="16.51" y2="-26.67" width="0.254" layer="94"/>
<wire x1="16.51" y1="-27.94" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="19.05" y2="-27.94" width="0.254" layer="94"/>
<wire x1="21.59" y1="-26.67" x2="22.86" y2="-26.67" width="0.254" layer="94"/>
<wire x1="22.86" y1="-26.67" x2="24.13" y2="-26.67" width="0.254" layer="94"/>
<wire x1="24.13" y1="-26.67" x2="22.86" y2="-27.94" width="0.254" layer="94"/>
<wire x1="22.86" y1="-27.94" x2="21.59" y2="-26.67" width="0.254" layer="94"/>
<wire x1="21.59" y1="-27.94" x2="22.86" y2="-27.94" width="0.254" layer="94"/>
<wire x1="22.86" y1="-27.94" x2="24.13" y2="-27.94" width="0.254" layer="94"/>
<wire x1="5.08" y1="22.86" x2="5.08" y2="20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="20.32" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="5.08" y2="-25.4" width="0.254" layer="94"/>
<wire x1="5.08" y1="-25.4" x2="2.54" y2="-25.4" width="0.254" layer="94"/>
<wire x1="2.54" y1="-25.4" x2="2.54" y2="-26.67" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="-25.4" width="0.254" layer="94"/>
<wire x1="10.16" y1="-25.4" x2="7.62" y2="-25.4" width="0.254" layer="94"/>
<wire x1="7.62" y1="-25.4" x2="7.62" y2="-26.67" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-2.54" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="-17.78" width="0.254" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="15.24" y2="-25.4" width="0.254" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="-26.67" width="0.254" layer="94"/>
<wire x1="20.32" y1="22.86" x2="20.32" y2="20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="12.7" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="-17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="-17.78" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="-25.4" x2="17.78" y2="-26.67" width="0.254" layer="94"/>
<wire x1="25.4" y1="22.86" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="20.32" x2="25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="12.7" x2="25.4" y2="5.08" width="0.254" layer="94"/>
<wire x1="25.4" y1="5.08" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-2.54" x2="25.4" y2="-10.16" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="25.4" y2="-25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="-25.4" x2="22.86" y2="-25.4" width="0.254" layer="94"/>
<wire x1="22.86" y1="-25.4" x2="22.86" y2="-26.67" width="0.254" layer="94"/>
<wire x1="5.08" y1="20.32" x2="2.54" y2="20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="20.32" x2="2.54" y2="19.05" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="7.62" y2="19.05" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="12.7" y2="20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="20.32" x2="12.7" y2="19.05" width="0.254" layer="94"/>
<wire x1="20.32" y1="20.32" x2="17.78" y2="20.32" width="0.254" layer="94"/>
<wire x1="17.78" y1="20.32" x2="17.78" y2="19.05" width="0.254" layer="94"/>
<wire x1="25.4" y1="20.32" x2="22.86" y2="20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="20.32" x2="22.86" y2="19.05" width="0.254" layer="94"/>
<wire x1="2.54" y1="11.43" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="11.43" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="11.43" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="11.43" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="12.7" x2="20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="11.43" x2="22.86" y2="12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="12.7" x2="25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="3.81" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="3.81" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="3.81" x2="17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="5.08" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="22.86" y1="3.81" x2="22.86" y2="5.08" width="0.254" layer="94"/>
<wire x1="22.86" y1="5.08" x2="25.4" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="-3.81" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-3.81" x2="17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-2.54" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-3.81" x2="22.86" y2="-2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-2.54" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-11.43" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-11.43" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-11.43" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-11.43" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="-11.43" x2="22.86" y2="-10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="25.4" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-19.05" x2="2.54" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-19.05" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-19.05" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="15.24" y2="-17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="-19.05" x2="17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="-17.78" x2="20.32" y2="-17.78" width="0.254" layer="94"/>
<wire x1="22.86" y1="-19.05" x2="22.86" y2="-17.78" width="0.254" layer="94"/>
<wire x1="22.86" y1="-17.78" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
<circle x="5.08" y="20.32" radius="0.254" width="0.8128" layer="94"/>
<circle x="2.54" y="15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="20.32" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="20.32" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="20.32" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="20.32" radius="0.254" width="0.8128" layer="94"/>
<circle x="5.08" y="12.7" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="12.7" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="12.7" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="12.7" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="12.7" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="5.08" y="5.08" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="5.08" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="5.08" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="5.08" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="5.08" radius="0.254" width="0.8128" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="-2.54" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="-2.54" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="-2.54" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="-2.54" radius="0.254" width="0.8128" layer="94"/>
<circle x="5.08" y="-10.16" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="-10.16" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="-10.16" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="-10.16" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="-10.16" radius="0.254" width="0.8128" layer="94"/>
<circle x="5.08" y="-17.78" radius="0.254" width="0.8128" layer="94"/>
<circle x="10.16" y="-17.78" radius="0.254" width="0.8128" layer="94"/>
<circle x="15.24" y="-17.78" radius="0.254" width="0.8128" layer="94"/>
<circle x="20.32" y="-17.78" radius="0.254" width="0.8128" layer="94"/>
<circle x="25.4" y="-17.78" radius="0.254" width="0.8128" layer="94"/>
<wire x1="0" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="22.86" y2="15.24" width="0.254" layer="94"/>
<wire x1="22.86" y1="15.24" x2="22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.78" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="10.16" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="22.86" y2="-7.62" width="0.254" layer="94"/>
<wire x1="22.86" y1="-7.62" x2="22.86" y2="-5.08" width="0.254" layer="94"/>
<wire x1="17.78" y1="-5.08" x2="17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="22.86" y2="-15.24" width="0.254" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="-22.86" x2="7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="7.62" y1="-22.86" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="17.78" y1="-22.86" x2="22.86" y2="-22.86" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.86" x2="22.86" y2="-20.32" width="0.254" layer="94"/>
<wire x1="17.78" y1="-20.32" x2="17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-30.48" x2="2.54" y2="-30.48" width="0.254" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="7.62" y2="-30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="-30.48" x2="12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="22.86" y2="-30.48" width="0.254" layer="94"/>
<wire x1="22.86" y1="-30.48" x2="22.86" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="7.62" y2="-30.48" width="0.254" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="2.54" y2="-30.48" width="0.254" layer="94"/>
<circle x="2.54" y="7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="2.54" y="0" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="0" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="0" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="0" radius="0.254" width="0.8128" layer="94"/>
<circle x="2.54" y="-7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="-7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="-7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="-7.62" radius="0.254" width="0.8128" layer="94"/>
<circle x="2.54" y="-15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="-15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="-15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="-15.24" radius="0.254" width="0.8128" layer="94"/>
<circle x="2.54" y="-22.86" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="-22.86" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="-22.86" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="-22.86" radius="0.254" width="0.8128" layer="94"/>
<wire x1="0" y1="22.86" x2="27.94" y2="22.86" width="0.4064" layer="94"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="27.94" y1="-33.02" x2="0" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="0" y1="-33.02" x2="0" y2="22.86" width="0.4064" layer="94"/>
<text x="2.54" y="24.13" size="1.27" layer="94">1</text>
<text x="7.62" y="24.13" size="1.27" layer="94">2</text>
<text x="12.7" y="24.13" size="1.27" layer="94">3</text>
<text x="17.78" y="24.13" size="1.27" layer="94">4</text>
<text x="22.86" y="24.13" size="1.27" layer="94">5</text>
<text x="-2.54" y="17.78" size="1.27" layer="94">1</text>
<text x="-2.54" y="10.16" size="1.27" layer="94">2</text>
<text x="-2.54" y="2.54" size="1.27" layer="94">3</text>
<text x="-2.54" y="-5.08" size="1.27" layer="94">4</text>
<text x="-2.54" y="-12.7" size="1.27" layer="94">5</text>
<text x="-2.54" y="-20.32" size="1.27" layer="94">6</text>
<text x="-2.54" y="-27.94" size="1.27" layer="94">7</text>
<text x="-2.54" y="24.13" size="1.27" layer="94">COL</text>
<text x="-5.08" y="20.32" size="1.27" layer="94">ROW</text>
<circle x="2.54" y="-30.48" radius="0.254" width="0.8128" layer="94"/>
<circle x="7.62" y="-30.48" radius="0.254" width="0.8128" layer="94"/>
<circle x="12.7" y="-30.48" radius="0.254" width="0.8128" layer="94"/>
<circle x="17.78" y="-30.48" radius="0.254" width="0.8128" layer="94"/>
<text x="0" y="30.48" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-35.56" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_MATRIX_5X7" prefix="LED">
<gates>
<gate name="G$1" symbol="LED_MATRIX_SYMBOL" x="2.54" y="30.48"/>
</gates>
<devices>
<device name="" package="LED_MATRIX_PACKAGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ebojconn">
<packages>
<package name="2X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.74625" x2="-3.81" y2="1.74625" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.74625" x2="3.81" y2="1.74625" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.74625" x2="3.81" y2="-1.74625" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.74625" x2="3.81" y2="-1.74625" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="2" x="-2.54" y="0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="4" x="-1.27" y="0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="5" x="0" y="-0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="6" x="0" y="0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="7" x="1.27" y="-0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="8" x="1.27" y="0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="9" x="2.54" y="-0.635" drill="0.65" diameter="1" shape="octagon"/>
<pad name="10" x="2.54" y="0.635" drill="0.65" diameter="1" shape="octagon"/>
<text x="-3.81" y="2.2225" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.4925" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PINH2X5">
<wire x1="-6.35" y1="-7.62" x2="8.89" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X5" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="LED1" library="Estudent-radionica" deviceset="LED_MATRIX_5X7" device=""/>
<part name="LED2" library="Estudent-radionica" deviceset="LED_MATRIX_5X7" device=""/>
<part name="JP1" library="ebojconn" deviceset="PINHD-2X5" device=""/>
<part name="JP2" library="ebojconn" deviceset="PINHD-2X5" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="63.5" y="101.6" size="1.778" layer="97">LED1</text>
<text x="139.7" y="99.06" size="1.778" layer="97">LED2</text>
</plain>
<instances>
<instance part="LED1" gate="G$1" x="96.52" y="101.6" smashed="yes" rot="R90"/>
<instance part="LED2" gate="G$1" x="172.72" y="101.6" smashed="yes" rot="R90"/>
<instance part="JP1" gate="A" x="99.06" y="58.42" smashed="yes">
<attribute name="NAME" x="92.71" y="66.675" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="A" x="175.26" y="58.42" smashed="yes">
<attribute name="NAME" x="168.91" y="66.675" size="1.778" layer="95"/>
<attribute name="VALUE" x="168.91" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="ROW7" class="0">
<segment>
<pinref part="JP2" gate="A" pin="8"/>
<wire x1="203.2" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<label x="200.66" y="55.88" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="12"/>
<wire x1="157.48" y1="99.06" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<label x="157.48" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW3" class="0">
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="78.74" y1="63.5" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<label x="81.28" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="9"/>
<wire x1="104.14" y1="99.06" x2="104.14" y2="83.82" width="0.1524" layer="91"/>
<label x="104.14" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW2" class="0">
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="78.74" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<label x="81.28" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="99.06" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<label x="96.52" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW1" class="0">
<segment>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="78.74" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<label x="81.28" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="11"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<label x="88.9" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW0" class="0">
<segment>
<pinref part="JP1" gate="A" pin="7"/>
<wire x1="78.74" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<label x="81.28" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="12"/>
<wire x1="81.28" y1="99.06" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<label x="81.28" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW6" class="0">
<segment>
<pinref part="JP1" gate="A" pin="9"/>
<wire x1="78.74" y1="53.34" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<label x="81.28" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="6"/>
<wire x1="127" y1="99.06" x2="127" y2="83.82" width="0.1524" layer="91"/>
<label x="127" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW5" class="0">
<segment>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="172.72" y1="63.5" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<label x="157.48" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="5"/>
<wire x1="119.38" y1="99.06" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<label x="119.38" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW4" class="0">
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="172.72" y1="60.96" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<label x="157.48" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="4"/>
<wire x1="111.76" y1="99.06" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<label x="111.76" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW8" class="0">
<segment>
<wire x1="203.2" y1="53.34" x2="180.34" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="10"/>
<label x="200.66" y="53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="11"/>
<wire x1="165.1" y1="99.06" x2="165.1" y2="83.82" width="0.1524" layer="91"/>
<label x="165.1" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW9" class="0">
<segment>
<pinref part="JP2" gate="A" pin="9"/>
<wire x1="172.72" y1="53.34" x2="154.94" y2="53.34" width="0.1524" layer="91"/>
<label x="157.48" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="2"/>
<wire x1="172.72" y1="99.06" x2="172.72" y2="83.82" width="0.1524" layer="91"/>
<label x="172.72" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW10" class="0">
<segment>
<pinref part="JP2" gate="A" pin="7"/>
<wire x1="172.72" y1="55.88" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
<label x="157.48" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="9"/>
<wire x1="180.34" y1="99.06" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<label x="180.34" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW11" class="0">
<segment>
<pinref part="JP2" gate="A" pin="5"/>
<wire x1="172.72" y1="58.42" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<label x="157.48" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="4"/>
<wire x1="187.96" y1="99.06" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
<label x="187.96" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW13" class="0">
<segment>
<pinref part="JP2" gate="A" pin="6"/>
<wire x1="203.2" y1="58.42" x2="180.34" y2="58.42" width="0.1524" layer="91"/>
<label x="200.66" y="58.42" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="6"/>
<wire x1="203.2" y1="99.06" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<label x="203.2" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ROW12" class="0">
<segment>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="203.2" y1="60.96" x2="180.34" y2="60.96" width="0.1524" layer="91"/>
<label x="200.66" y="60.96" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="5"/>
<wire x1="195.58" y1="99.06" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<label x="195.58" y="86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="COL1" class="0">
<segment>
<pinref part="JP1" gate="A" pin="8"/>
<wire x1="124.46" y1="55.88" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<label x="121.92" y="55.88" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="7"/>
<wire x1="71.12" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<wire x1="66.04" y1="121.92" x2="66.04" y2="137.16" width="0.1524" layer="91"/>
<wire x1="66.04" y1="137.16" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
<wire x1="144.78" y1="137.16" x2="144.78" y2="121.92" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="7"/>
<wire x1="144.78" y1="121.92" x2="147.32" y2="121.92" width="0.1524" layer="91"/>
<wire x1="66.04" y1="121.92" x2="43.18" y2="121.92" width="0.1524" layer="91"/>
<label x="45.72" y="121.92" size="1.778" layer="95"/>
<junction x="66.04" y="121.92"/>
</segment>
</net>
<net name="COL2" class="0">
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="124.46" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<label x="121.92" y="58.42" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="10"/>
<wire x1="71.12" y1="116.84" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
<wire x1="63.5" y1="116.84" x2="63.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="63.5" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
<wire x1="142.24" y1="139.7" x2="142.24" y2="116.84" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="10"/>
<wire x1="142.24" y1="116.84" x2="147.32" y2="116.84" width="0.1524" layer="91"/>
<wire x1="63.5" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<label x="45.72" y="116.84" size="1.778" layer="95"/>
<junction x="63.5" y="116.84"/>
</segment>
</net>
<net name="COL3" class="0">
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="104.14" y1="60.96" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
<label x="121.92" y="60.96" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="3"/>
<wire x1="71.12" y1="111.76" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<wire x1="60.96" y1="111.76" x2="60.96" y2="142.24" width="0.1524" layer="91"/>
<wire x1="60.96" y1="142.24" x2="139.7" y2="142.24" width="0.1524" layer="91"/>
<wire x1="139.7" y1="142.24" x2="139.7" y2="111.76" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="3"/>
<wire x1="139.7" y1="111.76" x2="147.32" y2="111.76" width="0.1524" layer="91"/>
<wire x1="60.96" y1="111.76" x2="43.18" y2="111.76" width="0.1524" layer="91"/>
<label x="45.72" y="111.76" size="1.778" layer="95"/>
<junction x="60.96" y="111.76"/>
</segment>
</net>
<net name="COL0" class="0">
<segment>
<pinref part="JP1" gate="A" pin="10"/>
<wire x1="124.46" y1="53.34" x2="104.14" y2="53.34" width="0.1524" layer="91"/>
<label x="121.92" y="53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="8"/>
<wire x1="71.12" y1="127" x2="68.58" y2="127" width="0.1524" layer="91"/>
<wire x1="68.58" y1="127" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="8"/>
<wire x1="68.58" y1="134.62" x2="147.32" y2="134.62" width="0.1524" layer="91"/>
<wire x1="147.32" y1="134.62" x2="147.32" y2="127" width="0.1524" layer="91"/>
<wire x1="68.58" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<label x="45.72" y="127" size="1.778" layer="95"/>
<junction x="68.58" y="127"/>
</segment>
</net>
<net name="COL4" class="0">
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="124.46" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<label x="121.92" y="63.5" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="1"/>
<wire x1="71.12" y1="106.68" x2="58.42" y2="106.68" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="144.78" x2="137.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="137.16" y1="144.78" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="1"/>
<wire x1="137.16" y1="106.68" x2="147.32" y2="106.68" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="43.18" y2="106.68" width="0.1524" layer="91"/>
<label x="45.72" y="106.68" size="1.778" layer="95"/>
<junction x="58.42" y="106.68"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
